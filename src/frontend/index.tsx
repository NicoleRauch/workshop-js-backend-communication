import "@picocss/pico/css/pico.min.css";
import "./index.css";

import { createRoot } from "react-dom/client";
import { App } from "./App.js";
import React from "react";

new EventSource("/esbuild").addEventListener("change", () => location.reload());

const host = document.getElementById("root");

if (host == null) {
  throw new Error("cannot find host to mount into, failing...");
}

const root = createRoot(host);

root.render(<App />);
