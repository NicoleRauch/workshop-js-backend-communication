import React from "react";

const pictures: { [species in Pet["species"]]: string } = {
  cat: "assets/cat.jpg",
  dog: "assets/dog.jpg",
  bird: "assets/canary.jpg",
  rabbit: "assets/rabbit.jpg",
  fish: "assets/fish.jpg",
};

export type Pet = {
  name: string;
  species: "cat" | "dog" | "bird" | "rabbit" | "fish";
  price: number;
};

/**
 * Form to admit new pets to the store
 */
export function Admission({ onSubmit }: { onSubmit: (pet: Pet) => void }) {
  const [form, setForm] = React.useState({
    name: "Pet",
    species: undefined as undefined | Pet["species"],
    price: 0,
  });

  const setField = <K extends keyof typeof form>(
    key: K,
    value: (typeof form)[K],
  ) => setForm((current) => ({ ...current, [key]: value }));

  return (
    <div>
      <h3>Add a New Pet</h3>
      <label>
        Pet name:
        <input
          type="text"
          required
          onBlur={(e) => setField("name", e.target.value)}
        />
      </label>
      <label>
        Pet price:
        <input
          type="number"
          step="0.01"
          min="0"
          required
          onBlur={(e) => setField("price", parseFloat(e.target.value))}
        />
      </label>

      <label>
        Species:
        <div className="grid">
          <button onClick={() => setField("species", "cat")}>
            <img height="50" src={pictures.cat} />
          </button>
          <button onClick={() => setField("species", "dog")}>
            <img height="50" src={pictures.dog} />
          </button>
          <button onClick={() => setField("species", "bird")}>
            <img height="50" src={pictures.bird} />
          </button>
          <button onClick={() => setField("species", "rabbit")}>
            <img height="50" src={pictures.rabbit} />
          </button>
          <button onClick={() => setField("species", "fish")}>
            <img height="50" src={pictures.fish} />
          </button>
        </div>
      </label>
      <div>
        <button
          disabled={form.species == null}
          onClick={() => {
            const { species, ...rest } = form;
            if (species == null) {
              return;
            }
            onSubmit({ species, ...rest });
          }}
        >
          Add {form.name}
          {form.species == null ? "" : `, a ${form.species}`}
        </button>
      </div>
    </div>
  );
}

/**
 * displays the given inventory in tabular form
 */
export function Inventory({
  pets,
  onSell,
}: {
  pets: Pet[];
  onSell: (pet: Pet) => void;
}) {
  return pets.length === 0 ? (
    <h4>Sorry, currently no pets for sale</h4>
  ) : (
    <div
      style={{
        display: "grid",
        gridTemplateRows: "auto",
        gridTemplateColumns: "100px 1fr 1fr",
        gap: "1rem",
        alignItems: "center",
      }}
    >
      {pets.map((pet) => (
        <React.Fragment key={pet.name}>
          <img height="20" src={pictures[pet.species]} />
          <h4 style={{ marginBottom: 0 }}>{pet.name}</h4>
          <button style={{ marginBottom: 0 }} onClick={() => onSell(pet)}>
            Sell {pet.name}
          </button>
        </React.Fragment>
      ))}
    </div>
  );
}

export function App() {
  const [pets, setPets] = React.useState(undefined as undefined | Pet[]);

  const fetchPets = () =>
    fetch(`http://localhost:3001/pets`)
      .then((res) => res.json())
      .then(
        (pets: Array<{ petName: string; petType: string; petPrice: number }>) =>
          pets.map(
            ({ petName, petPrice, petType }): Pet => ({
              name: petName,
              species: petType as Pet["species"],
              price: petPrice,
            }),
          ),
      )
      .then(setPets);

  React.useEffect(() => {
    fetchPets();
  }, []);

  const uploadPet = (pet: Pet) =>
    fetch(`http://localhost:3001/pets`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        petName: pet.name,
        petType: pet.species,
        petPrice: pet.price,
      }),
    }).then(fetchPets);

  const sellPet = (pet: Pet) =>
    fetch(`http://localhost:3001/pets`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        petName: pet.name,
        petType: pet.species,
      }),
    }).then(fetchPets);

  return (
    <main className="container">
      <h2>Welcome to our Pet Store</h2>
      <Admission onSubmit={uploadPet} />
      <h3>Pets in our Store</h3>
        {pets == undefined // loose equals so that we can handle accidental "null" values
         ? <div>Loading...</div>
         : <Inventory pets={pets} onSell={sellPet} />
        }
    </main>
  );
}
