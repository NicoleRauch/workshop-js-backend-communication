import * as Http from "node:http";

type Pet = {
  petName: string;
  petType: string;
  petPrice: number;
};

/**
 * "in memory" db
 */
let pets = [
  {
    petName: "Madam Pawsalicious De'Ville",
    petType: "cat",
    petPrice: 1000,
  },
  {
    petName: "Herbert von Hindenburg",
    petType: "fish",
    petPrice: 30,
  },
  {
    petName: "Honk",
    petType: "bird",
    petPrice: 100,
  },
  {
    petName: "Schurli",
    petType: "dog",
    petPrice: 500,
  },
  {
    petName: "Flitzer",
    petType: "rabbit",
    petPrice: 100,
  },
] as Pet[];

const bodyAsJson = async (req: Http.IncomingMessage) =>
  JSON.parse((await req.toArray()).join());

type IncomingServerMessage = Http.IncomingMessage & { url: string, method: string };

const handle = async (req: IncomingServerMessage, res: Http.ServerResponse) => {
  const url = new URL(req.url, `http://${req.headers.host}`);
  switch (url.pathname) {
    case "/pets": {
      switch (req.method) {
        case "GET": {
          res.setHeader("Content-Type", "application/json");
          res.write(JSON.stringify(pets));
          return res.end();
        }
        case "POST": {
          const pet = (await bodyAsJson(req)) as Pet;
          pets.push(pet);
          return res.end();
        }
        case "DELETE": {
          const pet = (await bodyAsJson(req)) as Pick<
            Pet,
            "petName" | "petType"
          >;
          pets = pets.filter(
              ({ petName, petType }) =>
                !(petName === pet.petName && petType === pet.petType),
          );
          return res.end();
        }
        case "OPTIONS": {
          return res.end();
        }
      }
    }
    default: {
      res.statusCode = 404;
      res.write("not found");
      return res.end();
    }
  }
};

const server = Http.createServer((req, res) => {
  try {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "*");
    res.setHeader("Access-Control-Allow-Methods", "*");
    handle(req as IncomingServerMessage, res);
  } catch {
    res.statusCode = 500;
    res.write("Internal Server Error");
    res.end();
  }
});

const port = 3001;
server.listen(port);
console.log(`backend up and running on :${port}`);
