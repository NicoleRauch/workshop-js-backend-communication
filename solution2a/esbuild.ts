import * as Esbuild from "esbuild";

const ctx = await Esbuild.context({
  bundle: true,
  target: "es2020",
  format: "esm",
  entryPoints: {
    index: "./frontend/index.tsx",
  },
  outdir: "./frontend/www/bundle",
  loader: {
    ".jpg": "file",
  },
});

await ctx.watch();

const { host, port } = await ctx.serve({
  port: 3000,
  servedir: "./frontend/www",
});

console.log(`frontend running at ${host}:${port}`);
