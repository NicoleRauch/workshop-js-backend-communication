import React from "react";
import { components, paths } from "../shared/openapi";

type Pet = components["schemas"]["pet"];

const pictures: { [species in Pet["species"]]: string } = {
  cat: "assets/cat.jpg",
  dog: "assets/dog.jpg",
  bird: "assets/canary.jpg",
  rabbit: "assets/rabbit.jpg",
  fish: "assets/fish.jpg",
};

const api = <path extends keyof paths>(
  path: path,
  method: keyof paths[path] & string,
  init?: RequestInit,
) =>
  fetch(`http://localhost:3001${path}`, {
    ...init,
    method,
  });

/**
 * Form to admit new pets to the store
 */
export function Admission({ onSubmit }: { onSubmit: (pet: Pet) => void }) {
  const [form, setForm] = React.useState({
    name: "Pet",
    species: undefined as undefined | Pet["species"],
    price: 0,
  });

  const setField = <K extends keyof typeof form>(
    key: K,
    value: (typeof form)[K],
  ) => setForm((current) => ({ ...current, [key]: value }));

  return (
    <div>
      <h3>Add a New Pet</h3>
      <label>
        Pet name:
        <input
          type="text"
          required
          onBlur={(e) => setField("name", e.target.value)}
        />
      </label>
      <label>
        Pet price:
        <input
          type="number"
          step="0.01"
          min="0"
          required
          onBlur={(e) => setField("price", parseFloat(e.target.value))}
        />
      </label>

      <label>
        Species:
        <div className="grid">
          <button onClick={() => setField("species", "cat")}>
            <img height="50" src={pictures.cat} />
          </button>
          <button onClick={() => setField("species", "dog")}>
            <img height="50" src={pictures.dog} />
          </button>
          <button onClick={() => setField("species", "bird")}>
            <img height="50" src={pictures.bird} />
          </button>
          <button onClick={() => setField("species", "rabbit")}>
            <img height="50" src={pictures.rabbit} />
          </button>
          <button onClick={() => setField("species", "fish")}>
            <img height="50" src={pictures.fish} />
          </button>
        </div>
      </label>
      <div>
        <button
          disabled={form.species == null}
          onClick={() => {
            const { species, ...rest } = form;
            if (species == null) {
              return;
            }
            onSubmit({ species, ...rest });
          }}
        >
          Add {form.name}
          {form.species == null ? "" : `, a ${form.species}`}
        </button>
      </div>
    </div>
  );
}

/**
 * displays the given inventory in tabular form
 */
export function Inventory({
  pets,
  onSell,
}: {
  pets: Pet[];
  onSell: (pet: Pet) => void;
}) {
  return pets.length === 0 ? (
    <h4>Sorry, currently no pets for sale</h4>
  ) : (
    <div
      style={{
        display: "grid",
        gridTemplateRows: "auto",
        gridTemplateColumns: "100px 1fr 1fr",
        gap: "1rem",
        alignItems: "center",
      }}
    >
      {pets.map((pet) => (
        <React.Fragment key={pet.name}>
          <img height="20" src={pictures[pet.species]} />
          <h4 style={{ marginBottom: 0 }}>{pet.name}</h4>
          <button style={{ marginBottom: 0 }} onClick={() => onSell(pet)}>
            Sell {pet.name}
          </button>
        </React.Fragment>
      ))}
    </div>
  );
}

export function App() {
  const [pets, setPets] = React.useState(undefined as undefined | Pet[]);

  const fetchPets = () =>
    api("/pets", "get")
      .then((res) => res.json())
      .then(
        (pets: components["schemas"]["pet"][]) =>
          pets.map(
            ({ name, price, species }): Pet => ({
              name,
              species: species as Pet["species"],
              price,
            }),
          ),
      )
      .then(setPets);

  React.useEffect(() => {
    fetchPets();
  }, []);

  const uploadPet = (pet: Pet) =>
    api("/pets", "post", {
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(pet),
    }).then(fetchPets);

  const sellPet = (pet: Pet) =>
    api("/pets", "delete", {
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        name: pet.name,
        species: pet.species,
      }),
    }).then(fetchPets);

  return (
    <main className="container">
      <h2>Welcome to our Pet Store</h2>
      <Admission onSubmit={uploadPet} />
      <h3>Pets in our Store</h3>
      <Inventory pets={pets ?? []} onSell={sellPet} />
    </main>
  );
}
