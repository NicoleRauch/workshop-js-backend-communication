import * as Http from "node:http";
import { components, paths } from "../shared/openapi";

type Pet = components["schemas"]["pet"];

/**
 * "in memory" db
 */
const pets = [
  {
    name: "Madam Pawsalicious De'Ville",
    species: "cat",
    price: 1000,
  },
  {
    name: "Herbert von Hindenburg",
    species: "fish",
    price: 30,
  },
  {
    name: "Honk",
    species: "bird",
    price: 100,
  },
  {
    name: "Schurli",
    species: "dog",
    price: 500,
  },
  {
    name: "Flitzer",
    species: "rabbit",
    price: 100,
  },
] as Pet[];

const bodyAsJson = async (req: Http.IncomingMessage) =>
  JSON.parse((await req.toArray()).join());

type IncomingServerMessage = Http.IncomingMessage & {
  url: string;
  method: string;
};

type Handler = (req: IncomingServerMessage, res: Http.ServerResponse) => void;

const handlers: {
  [path in keyof paths]: {
    [method in keyof paths[path]]: Handler;
  };
} = {
  "/pets": {
    get: (_, res) => {
      res.setHeader("Content-Type", "application/json");
      res.write(JSON.stringify(pets));
      return res.end();
    },
    post: async (req, res) => {
      const pet = (await bodyAsJson(req)) as Pet;
      pets.push(pet);
      return res.end();
    },
    delete: async (req, res) => {
      const pet = (await bodyAsJson(req)) as Pick<Pet, "name" | "species">;
      pets.splice(
        pets.findIndex(
          ({ name, species }) =>
            name === pet.name && species === pet.species,
        ),
        1,
      );
      return res.end();
    },
  },
};

const handle = async (req: IncomingServerMessage, res: Http.ServerResponse) => {
  const url = new URL(req.url!, `http://${req.headers.host}`);
  const handler =
    (handlers as Record<string, Record<string, Handler>>)[url.pathname]?.[
      req.method.toLowerCase()
    ] ?? (req.method === "OPTIONS" ? () => res.end() : undefined);

  if (handler == null) {
    res.statusCode = 404;
    res.write("not found");
    return res.end();
  }

  return handler(req, res);
};

const server = Http.createServer((req, res) => {
  try {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "*");
    res.setHeader("Access-Control-Allow-Methods", "*");
    handle(req as IncomingServerMessage, res);
  } catch {
    res.statusCode = 500;
    res.write("Internal Server Error");
    res.end();
  }
});

const port = 3001;
server.listen(port);
console.log(`backend up and running on :${port}`);
