const EXPECTED_VERSION = ["20", "21"];
const nodeVersion = process.version.replace(/^v/, '');
const [ nodeMajorVersion ] = nodeVersion.split('.');

if (!EXPECTED_VERSION.includes(nodeMajorVersion)) {
  console.warn(`node version ${nodeVersion} is incompatible with this module. ` +
      `Expected version ${EXPECTED_VERSION}`);
  process.exit(1);
}
